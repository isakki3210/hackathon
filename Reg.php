<?php
require './_templates/head.php';
require './_templates/header.php';
require './includes/Database.class.php';
require './includes/Registration.class.php';
require './includes/Session.class.php';

Session::start();
$conn = Database::getConnection();
$registration = new Registration();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST['otp'])) {
    if ($_POST['otp'] == $_SESSION['otp']) {
      $registration = $registration->registerEvent($conn);
      header("Location: registration_success.php?msg=" . urlencode($registration));
      exit();
    } else {
      header("Location: registration_success.php?msg=otp");
    }
  }
}
$conn->close();
?>

<style>
  .cc {
    padding: 30px;
    border-radius: 8px;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
  }

  .btn-primary {
    background-color: #007bff;
    border-color: #007bff;
  }

  .btn-primary:hover {
    background-color: #0056b3;
    border-color: #0056b3;
  }

  .form-label {
    font-weight: bold;
  }

  .is-invalid {
    border: 1px solid #dc3545;
  }
</style>
<br /><br />
<div class="container cc mt-5 col-md-6">
  <h2 class="mb-4 text-center">Event Registration</h2>
  <!-- Single Form with Steps -->
  <form id="registration-form" method="post" enctype="multipart/form-data">
    <!-- Step 1: Basic Information -->
    <div id="step1" class="registration-step">
      <div class="mb-3">
        <label for="name" class="form-label">Team Leader Name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Team Leader Name" required />
      </div>

      <div class="mb-3">
        <label class="form-label">Team Member Names</label>
        <input type="text" class="form-control" name="team_member_1_name" placeholder="Team Member 1 Name" required />
      </div>

      <div class="mb-3">
        <input type="text" class="form-control" name="team_member_2_name" placeholder="Team Member 2 Name" required />
      </div>

      <div class="mb-3">
        <input type="text" class="form-control" name="team_member_3_name" placeholder="Team Member 3 Name (Optional)" />
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="example@example.com" required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="mb-3">
            <label for="phone" class="form-label">Phone Number</label>
            <input type="tel" class="form-control" id="phone" name="phone" placeholder="12367-45678" required />
          </div>
        </div>
      </div>
      <div class="mb-3">
        <label for="institution" class="form-label">Institution Name</label>
        <input type="text" class="form-control" id="institution" name="institution" placeholder="Institution name" required />
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="mb-3">
            <label for="degree" class="form-label">Degree</label>
            <!-- <input type="text" class="form-control" id="degree" name="degree" placeholder="Degree" required /> -->
            <select name="degree" id="" class="form-select" required>
              <option value="" disabled selected>Select a Degree</option>
              <option value="B.E.">B.E.</option>
              <option value="B.Tech">B.Tech</option>
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="mb-3">
            <label for="branch" class="form-label">Branch</label>
            <select name="branch" id="" class="form-select" required>
              <option value="" disabled selected>Select a Branch</option>
              <option value="CSE">CSE</option>
              <option value="IT">IT</option>
              <option value="AI & DS">AI & DS</option>
              <option value="CSD">CSD</option>
              <option value="ECE">ECE</option>
              <option value="EIE">EIE</option>
              <option value="BME">BME</option>
              <option value="Robotics">Robotics</option>
              <option value="EEE">EEE</option>
              <option value="Agri">Agri</option>
              <option value="Chem">Chem</option>
              <option value="Mech">Mech</option>
              <option value="others">Others</option>
            </select>
          </div>
        </div>
      </div>
      <!-- <div class="mb-3">
        <label for="institution" class="form-label">If Others Specify that Branch</label>
        <input type="text" class="form-control" id="institution" name="institution" placeholder="Branch (Optional)" required />
      </div> -->
      <div class="mb-3">
        <label for="theme" class="form-label">Select a Theme</label>
        <select class="form-select" id="theme" name="theme" required>
          <option value="" disabled selected>Select a Theme</option>
          <?php
          $Themes = array(
            "Smart Cities and Urban Development",
            "Healthcare and Medical Technology",
            "Agriculture and Rural Development",
            "Algorithmic Challenges",
            "Artificial Intelligence (AI) for Social Good",
            "Virtual Reality (VR) and Augmented Reality (AR) Experiences",
            "Accessibility and Inclusion Tech",
            "Open Innovation Challenge",
          );
          foreach ($Themes as $theme) {
          ?>
            <option value="<?php echo $theme; ?>"><?php echo $theme; ?></option>
          <?php } ?>
        </select>
      </div>
      <!-- <div class="col-sm-4">
        <label for="ppt" class="col-form-label">Download PPT Template</label> <br>
        <a href="../?>" download class="mt-5">
          Download PPT
        </a>
      </div> -->
      <div class="text-center">
        <button type="button" class="btn btn-primary" onclick="sendOTPAndNextStep()">
          Next
        </button>
      </div>
    </div>
    <!-- Step 2: OTP Verification and Screenshot Upload -->
    <div id="step2" class="registration-step" style="display: none">
      <div class="mb-3">
        <label for="otp" class="form-label">Enter OTP</label>
        <small id="otpMessage">OTP will be sent to your given Email.</small>
        <input type="text" class="form-control" id="otp" name="otp" placeholder="Enter OTP" required />
      </div>
      <div class="mb-3">
        <label for="totalPayment" class="form-label">Total Payment</label>
        <input type="text" class="form-control" id="totalPayment" readonly />
      </div>
      <div class="mb-3">
        <h3>Bank Details for Payment</h3>
        <p>Please make the payment to the following bank account:</p>
        <ul>
          <li>The Principal,</li>
          <li>Erode Sengunthar Engineering College</li>
          <li>Account Number: 500101012422890</li>
          <li>City Union Bank, Erode Branch.</li>
          <li>IFSC Code: CIUB0000059</li>
        </ul>
      </div>
      <div class="mb-3">
        <label for="totalPayment" class="form-label">Transaction Id</label>
        <input type="text" class="form-control" placeholder="Transaction Id" id="transactionId" name="transaction_id" />
        <small id="value"></small>
      </div>
      <div class="mb-3">
        <label for="image" class="form-label">Upload a Screenshot of the Payment</label>
        <input type="file" name="image" class="form-control" id="image" accept="image/*" required />
      </div>

      <div class="text-center">
        <button type="button" class="btn btn-secondary" onclick="prevStep(1)">
          Back
        </button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-beta1/js/bootstrap.bundle.min.js"></script>
<script src="./assets/js/Reg.js"></script>
<script>
  const transactionIdInput = document.getElementById('transactionId');
  const validationMessage = document.getElementById('value');

  transactionIdInput.addEventListener('input', function() {
    const transactionId = transactionIdInput.value.trim();
    const pattern = /^3\d{0,11}$/;
    if (pattern.test(transactionId)) {
      transactionIdInput.style.borderColor = 'green';
      validationMessage.textContent = 'Valid Transaction Id';
      validationMessage.style.color = 'green';
    } else {
      // Invalid input, set the input field to red
      transactionIdInput.style.borderColor = 'red';
      validationMessage.textContent = 'Invalid Transaction Id. Please enter a valid Transaction Id';
      validationMessage.style.color = 'red';
    }
  });

  function sendOTP() {

    const email = document.getElementById('email').value;
    fetch('generate_otp.php', {
        method: 'POST',
        body: new URLSearchParams({
          email
        }),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => response.text())
      .then((data) => {
        if (data === 'OTP sent successfully.') {
          // OTP sent successfully, show Step 2
          document.getElementById('step1').style.display = 'none';
          document.getElementById('step2').style.display = 'block';
        } else {
          // Failed to send OTP, show an error message
          alert(data);
        }
      });
  }

  function sendOTPAndNextStep() {
    sendOTP();
    nextStep(2);
  }
</script>
</body>

</html>