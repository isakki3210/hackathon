<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Registration
{
    public function moveUploadedFile($fieldName, $targetDirectory)
    {
        if (!isset($_FILES[$fieldName]) || !is_uploaded_file($_FILES[$fieldName]['tmp_name'])) {
            return false;
        }
        $targetPath = $targetDirectory . basename($_FILES[$fieldName]['name']);
        if (move_uploaded_file($_FILES[$fieldName]['tmp_name'], $targetPath)) {
            return $targetPath;
        } else {
            return false;
        }
    }

    public static function generateOTP()
    {
        return rand(1000, 9999);
    }

    public static function generateUniqueID($conn)
    {
        $uniqueID = "";
        do {
            $uniqueID = str_pad(mt_rand(0, 9999), 4, '0', STR_PAD_LEFT);

            $query = "SELECT COUNT(*) as count FROM registrations WHERE unique_id = '$uniqueID'";
            $result = $conn->query($query);

            if ($result) {
                $row = $result->fetch_assoc();
                $count = $row['count'];
                $result->close();
            } else {
                die("Database query error: " . $conn->error);
            }
        } while ($count > 0);
        return $uniqueID;
    }


    public static function SendOtp($email, $otp)
    {
        require './vendor/autoload.php';
        $mail = new PHPMailer(true);
        try {
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'skillathoncse@gmail.com';
            $mail->Password = 'xfpf udsa deuw hjdy';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->setFrom('skillathoncse@gmail.com', 'Skillathon2k23');
            $mail->addAddress($email, "");
            $mail->isHTML(true);
            $mail->Subject = "Your OTP for Event Registration";
            $mail->Body = '
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your OTP for Event Registration</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }
        h1 {
            color: #007bff;
        }
        p {
            font-size: 16px;
        }
        .otp {
            background-color: #007bff;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            font-size: 18px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Your OTP for Event Registration</h1>
        <p>Thank you for registering for our event. To complete your registration, please enter the OTP (One-Time Password) provided below in the registration form:</p>
        <div class="otp">' . $otp . '</div>
        <p>This OTP is valid for a limited time and should be used to verify your registration. If you didn\'t initiate this registration or have any concerns, please contact us immediately.</p>
        <p>We look forward to your participation in our event!</p>
        <p>Best regards,<br>Team SkillaThon 2k23</p>
    </div>
</body>
</html>';

            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }


    public static function SendMail($recipientEmail, $uniqueId, $name)
    {
        require './vendor/autoload.php';
        $mail = new PHPMailer(true);
        try {
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'skillathoncse@gmail.com';
            $mail->Password = 'xfpf udsa deuw hjdy';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->setFrom('skillathoncse@gmail.com', 'Skillathon2k23');
            $mail->addAddress($recipientEmail, $name);
            $mail->isHTML(true);
            $mail->Subject = 'State-Level Hackathon Registration Successful!';
            $mail->Body = '
    <html>
    <head>
        <style>
            body {
                font-family: Arial, sans-serif;
            }
            .container {
                max-width: 600px;
                margin: 0 auto;
                padding: 20px;
                text-align: center;
            }
            .header {
                background-color: #007BFF;
                color: #fff;
                padding: 10px;
            }
            .headers {
                text-align: left;
                padding: 20px;
            }
            .content {
                padding: 20px;
                text-align: left;
                color:white;
            }
            .highlight , .span{
                color: red;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <h1>State-Level Hackathon Registration</h1>
            </div>
            <div class="content">
                <p>We are Happy to inform you that your registration for the State-Level Hackathon has been successfully completed.</p>
                <p>SkillaThon - 2k23 : Conducted by Department of Computer Science and Engineering at Erode Sengunthar Engineering College</p>
                <p> Your unique participant ID is: <span class="span">SKLN ' . $uniqueId . '</span></p>
                <p>Congratulations! You are now officially part of this exciting competition. Get ready to showcase your skills, collaborate with fellow enthusiasts, and take on challenges that will push your limits.</p>
                <p class="highlight">Please keep an eye on your inbox, as we will be sending a confirmation email shortly.</p>
                <p>Once again, welcome to the State-Level Hackathon, and best of luck in the upcoming competition!</p>
            </div>
            <div class="headers">
            <p>With Regards</p>
            <p>Team SkillaThon 2K23</p>
            <p>Department of Computer Science and Engineering,</p>
            <p>Erode Sengunthar Engineering College.</p>
            <p>For Further Enquires, Contact</p>
            <p>B.karthikeyan IV CSE - 9361328417</p>
            <p>A. Tharun Kumar IV CSE - 7603821703</p>
            </div>
        </div>
    </body>
    </html>';
            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }


    public function registerEvent($conn)
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $uploadDirectory = 'uploads/';
            $imageTargetPath = $this->moveUploadedFile('image', $uploadDirectory);

            if (!$imageTargetPath) {
                echo "Error uploading files.";
            } else {
                $team_leader_name = $_POST['name'];
                $team_member_1_name = $_POST['team_member_1_name'];
                $team_member_2_name = $_POST['team_member_2_name'];
                $team_member_3_name = $_POST['team_member_3_name'];
                $email = $_POST['email'];
                $phone = $_POST['phone'];
                $institution_name = $_POST['institution'];
                $branch = $_POST['branch'];
                $degree = $_POST['degree'];
                $theme = $_POST['theme'];
                $transaction_id = $_POST['transaction_id'];

                $uniqueID = self::generateUniqueID($conn);

                $emailCheck = "SELECT COUNT(*) as email_count FROM registrations WHERE email = '$email'";
                $phoneCheck = "SELECT COUNT(*) as phone_count FROM registrations WHERE phone = '$phone'";
                $transaction_id_check = "SELECT COUNT(*) as transaction_count FROM registrations WHERE transaction_id = '$transaction_id'";

                $emailResult = $conn->query($emailCheck);
                $phoneResult = $conn->query($phoneCheck);
                $transaction_res = $conn->query($transaction_id_check);

                $emailCount = $emailResult->fetch_assoc()['email_count'];
                $phoneCount = $phoneResult->fetch_assoc()['phone_count'];
                $transaction_id_count = $transaction_res->fetch_assoc()['transaction_count'];

                if ($emailCount > 0) {
                    return "email";
                } elseif ($transaction_id_count > 0) {
                    return "transaction_id";
                } elseif ($phoneCount > 0) {
                    return "phone";
                } else {
                    $nameCheck = "SELECT COUNT(*) as name_count FROM registrations WHERE (team_leader_name = '$team_leader_name' OR team_member_1_name = '$team_leader_name' OR team_member_2_name = '$team_leader_name' OR team_member_3_name = '$team_leader_name') AND institution_name = '$institution_name'";
                    $nameResult = $conn->query($nameCheck);
                    $nameCount = $nameResult->fetch_assoc()['name_count'];

                    if ($nameCount > 0) {
                        return "name";
                    } else {
                        $sql = "INSERT INTO registrations (unique_id, team_leader_name, team_member_1_name, team_member_2_name, team_member_3_name, email, phone, institution_name, branch, degree, theme,transaction_id, payment_screenshot_filename)
                            VALUES ('$uniqueID', '$team_leader_name', '$team_member_1_name', '$team_member_2_name', '$team_member_3_name', '$email', '$phone', '$institution_name', '$branch', '$degree', '$theme', '$transaction_id', '$imageTargetPath')";
                        if ($conn->query($sql) === TRUE) {
                            self::SendMail($email, $uniqueID, $team_leader_name);
                            return $uniqueID;
                        } else {
                            return "fail";
                        }
                    }
                }
            }
        }
    }
}
