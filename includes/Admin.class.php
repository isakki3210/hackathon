<?php
class Admin
{
    private static $conn;

    public static function setConnection($conn)
    {
        self::$conn = $conn;
    }

    public static function getRegistrations()
    {
        $sql = "SELECT * FROM registrations";
        $result = self::$conn->query($sql);

        if ($result->num_rows > 0) {
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return [];
        }
    }
    public static function getSingleRegistrations($id)
    {
        $sql = "SELECT * FROM registrations WHERE unique_id = $id";
        $result = self::$conn->query($sql);

        if ($result->num_rows == 1) {
            return $result->fetch_assoc();
        } else {
            return "error";
        }
    }
    public static function DeleteRegistration($id)
    {
        $sql = "DELETE FROM registrations WHERE unique_id = $id";
        $result = self::$conn->query($sql);

        if ($result->num_rows == 1) {
            return $result->fetch_assoc();
        } else {
            return "error";
        }
    }

    public static function TotalCount()
    {
        $sql = "SELECT count(*) as count FROM registrations";
        $result = self::$conn->query($sql);

        if ($result !== false) {
            $row = $result->fetch_assoc();
            $count = $row['count'];
            return $count;
        } else {
            echo "Error: " . self::$conn->error;
            return 0;
        }
    }

    public static function calculateTotalAmount()
    {
        $sql = "SELECT team_leader_name, team_member_1_name, team_member_2_name, team_member_3_name FROM registrations";
        $result = self::$conn->query($sql);

        if ($result) {
            $totalAmount = 0;

            while ($row = $result->fetch_assoc()) {
                if (!empty($row['team_leader_name'])) {
                    $totalAmount += 200;
                }
                if (!empty($row['team_member_1_name'])) {
                    $totalAmount += 200;
                }
                if (!empty($row['team_member_2_name'])) {
                    $totalAmount += 200;
                }
                if (!empty($row['team_member_3_name'])) {
                    $totalAmount += 200;
                }
            }

            return $totalAmount;
        } else {
            return 0;
        }
    }
    public static function calculateUserAmount($unique)
    {
        $sql = "SELECT team_leader_name, team_member_1_name, team_member_2_name, team_member_3_name FROM registrations WHERE unique_id = '$unique'";
        $result = self::$conn->query($sql);

        if ($result) {
            $totalAmount = 0;
            $row = $result->fetch_assoc();
            if (!empty($row['team_leader_name'])) {
                $totalAmount += 200;
            }
            if (!empty($row['team_member_1_name'])) {
                $totalAmount += 200;
            }
            if (!empty($row['team_member_2_name'])) {
                $totalAmount += 200;
            }
            if (!empty($row['team_member_3_name'])) {
                $totalAmount += 200;
            }
            return $totalAmount;
        } else {
            return 0;
        }
    }
    public static function generatePDF()
    {
        if (!function_exists('get_magic_quotes_runtime')) {
            function get_magic_quotes_runtime()
            {
                return 0;
            }
        }
        require('fpdf/fpdf.php');
        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial', '', 5);

        // Define the header row for your PDF table

        $pdf->Cell(12.5, 6, 'UniqueId', 1);
        $pdf->Cell(20.5, 6, 'Leader Name', 1);
        $pdf->Cell(20.5, 6, 'Member 1', 1);
        $pdf->Cell(20.5, 6, 'Member 2', 1);
        $pdf->Cell(20.5, 6, 'Member 3', 1);
        $pdf->Cell(35.5, 6, 'Email', 1);
        $pdf->Cell(15.5, 6, 'Phone', 1);
        $pdf->Cell(40.5, 6, 'Institution', 1);


        // Retrieve data from the database (replace with your SQL query)
        $sql = "SELECT unique_id, team_leader_name, team_member_1_name, team_member_2_name, team_member_3_name, institution_name, email, phone FROM registrations";
        $result = self::$conn->query($sql);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $pdf->Ln();
                $pdf->Cell(12.5, 6, $row['unique_id'], 1);
                $pdf->Cell(20.5, 6, $row['team_leader_name'], 1);
                $pdf->Cell(20.5, 6, $row['team_member_1_name'], 1);
                $pdf->Cell(20.5, 6, $row['team_member_2_name'], 1);
                $pdf->Cell(20.5, 6, $row['team_member_3_name'], 1);
                $pdf->Cell(35.5, 6, $row['email'], 1);
                $pdf->Cell(15.5, 6, $row['phone'], 1);
                $pdf->Cell(40.5, 6, $row['institution_name'], 1);
            }
        } else {
            $pdf->Ln();
            $pdf->Cell(380, 6, 'No records found.', 1, 1, 'C');
        }

        $pdf->Output('Skillathon.pdf', 'D');
    }
    public static function generatePDFTransaction()
    {
        if (!function_exists('get_magic_quotes_runtime')) {
            function get_magic_quotes_runtime()
            {
                return 0;
            }
        }
        require('fpdf/fpdf.php');
        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial', '', 8);

        // Define the header row for your PDF table

        $pdf->Cell(25.5, 6, 'UniqueId', 1);
        $pdf->Cell(32.5, 6, 'Leader Name', 1);
        $pdf->Cell(55.5, 6, 'Email', 1);
        $pdf->Cell(25.5, 6, 'Phone', 1);
        $pdf->Cell(20.5, 6, 'Amount', 1);
        $pdf->Cell(26.5, 6, 'Transaction Id', 1);


        // Retrieve data from the database (replace with your SQL query)
        $sql = "SELECT unique_id, team_leader_name, email , phone , transaction_id FROM registrations";
        $result = self::$conn->query($sql);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $pdf->Ln();
                $pdf->Cell(25.5, 6, $row['unique_id'], 1);
                $pdf->Cell(32.5, 6, $row['team_leader_name'], 1);
                $pdf->Cell(55.5, 6, $row['email'], 1);
                $pdf->Cell(25.5, 6, $row['phone'], 1);
                $pdf->Cell(20.5, 6, self::calculateUserAmount($row['unique_id']), 1);
                $pdf->Cell(26.5, 6, $row['transaction_id'], 1);
            }
        } else {
            $pdf->Ln();
            $pdf->Cell(380, 6, 'No records found.', 1, 1, 'C');
        }

        $pdf->Output('Transaction_details.pdf', 'D');
    }
}
