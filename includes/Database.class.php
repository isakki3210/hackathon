<?php
class Database
{
    private static $conn = null;
    public static function getConnection()
    {
        if (self::$conn == null) {
            $db_host = 'localhost';
            $db_user = 'root';
            $db_pass = '';
            $db_name = 'event_registration';

            self::$conn = new mysqli($db_host, $db_user, $db_pass, $db_name);

            if (self::$conn->connect_error) {
                die("Connection failed: " . self::$conn->connect_error);
            }
        }
        return self::$conn;
    }
}
