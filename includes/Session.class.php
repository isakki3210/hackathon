<?php
class Session
{
    public static function start()
    {
        session_start();
    }

    public static function login($username)
    {
        $_SESSION['authenticated'] = true;
        $_SESSION['username'] = $username;
    }

    public static function logout()
    {
        session_unset();
        session_destroy();
    }

    public static function isAuthenticated()
    {
        return isset($_SESSION['authenticated']) && $_SESSION['authenticated'] === true;
    }

    public static function getUsername()
    {
        return isset($_SESSION['username']) ? $_SESSION['username'] : null;
    }
}
