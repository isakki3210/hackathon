<?php
require './includes/Registration.class.php';
require './includes/Session.class.php';

Session::start();

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $email = $_POST['email'];
    $otp = Registration::generateOTP();
    $_SESSION['otp'] = $otp;
    $emailSent = Registration::SendOtp($email, $_SESSION['otp']);
    if ($emailSent) {
        echo 'OTP sent successfully.';
    }
} else {
    echo 'Invalid request method.';
}
