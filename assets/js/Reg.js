let currentStep = 1;

function nextStep(step) {
  if (step === 2 && validateStep(currentStep)) {
    showStep(step);
    if (step === 2) {
      calculateTotalPayment();
    }
  }
}

function prevStep(step) {
  showStep(step);
}

function validateStep(step) {
  if (step === 1) {
    const requiredFields = document.querySelectorAll("#step1 [required]");
    let isStep1Valid = true;
    for (const field of requiredFields) {
      if (!field.value.trim()) {
        isStep1Valid = false;
        field.classList.add("is-invalid");
      } else {
        field.classList.remove("is-invalid");
      }
    }
    return isStep1Valid;
  }
  return true;
}

function calculateTotalPayment() {
  const teamLeaderName = document.querySelector(
    'input[placeholder="Team Leader Name"]'
  ).value;
  const teamMember1Name = document.querySelector(
    'input[placeholder="Team Member 1 Name"]'
  ).value;
  const teamMember2Name = document.querySelector(
    'input[placeholder="Team Member 2 Name"]'
  ).value;
  const teamMember3Name = document.querySelector(
    'input[placeholder="Team Member 3 Name (Optional)"]'
  ).value;

  const numberOfTeamMembers = [
    teamMember1Name,
    teamMember2Name,
    teamMember3Name,
    teamLeaderName,
  ].filter((name) => name.trim() !== "").length;

  const paymentPerHead = 200;
  const totalPayment = numberOfTeamMembers * paymentPerHead;
  document.getElementById("totalPayment").value =
    "Rs " + totalPayment.toFixed(2);
}

function showStep(step) {
  const steps = document.getElementsByClassName("registration-step");
  for (let i = 0; i < steps.length; i++) {
    steps[i].style.display = "none";
  }
  document.getElementById(`step${step}`).style.display = "block";
  currentStep = step;
}
