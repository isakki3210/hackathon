<?php
require './_login_head.php';
require '../includes/User.class.php';
require '../includes/Session.class.php';

Session::start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $user = new User($username, $password);

    if ($user->authenticate()) {
        Session::login($username);
        header('Location: index.php');
        exit();
    } else {
        echo 'Invalid username or password';
    }
}

?>

<body>
    <div class="container login-container">
        <form class="col-md-4 login-form" method="post" action="login.php">
            <div class="text-center logo">
                <img src="../assets/img/main.png" alt="Logo" width="150">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="username">
            </div>
            <div class="form-group mt-2">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
            </div>
            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-beta1/js/bootstrap.bundle.min.js"></script>
</body>

</html>