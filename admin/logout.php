<?php
include '../includes/Session.class.php';

Session::start();

if (Session::isAuthenticated()) {
    Session::logout();
    header('Location: login.php');
    exit();
} else {
    header('Location: login.php');
    exit();
}
