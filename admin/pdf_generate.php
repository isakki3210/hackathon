<?php
require '../includes/Admin.class.php';
require '../includes/Database.class.php';

$conn = Database::getConnection();
Admin::setConnection($conn);

if (isset($_GET['download_pdf'])) {
    Admin::generatePDF();
}
if (isset($_GET['download_transaction'])) {
    Admin::generatePDFTransaction();
}
