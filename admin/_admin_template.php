<?php
include '../includes/Session.class.php';

Session::start();

if (!Session::isAuthenticated()) {
    header('Location: ../index.php');
    exit();
} ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-beta1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link href="../assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="../assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">
    <title>Admin Panel</title>
    <style>
        .table-card {
            border: none;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
            /* Added box shadow */
            border-radius: 10px;
        }

        .table-card .card-header {
            background-color: #f2f2f2;
            font-weight: bold;
        }

        .table-card .table {
            margin-bottom: 0;
        }

        .table-card .table tbody tr:hover {
            background-color: #f5f5f5;
        }

        .logout-button {
            background-color: #d9534f;
            color: #fff;
            border: none;
            border-radius: 8px;
            padding: 10px 10px;
            margin-top: 10px;
            cursor: pointer;
        }

        .logout-button:hover {
            background-color: #c9302c;
        }

        /* Added styles for Total Payment and Total Users cards */
        .info-card {
            border: none;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
            /* Added box shadow */
            border-radius: 10px;
            text-align: center;
            padding: 20px 0;
            margin-bottom: 20px;
        }

        .info-card i {
            font-size: 24px;
        }

        .info-card h5 {
            margin-top: 10px;
        }
    </style>
</head>

<body>
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center justify-content-between">
            <div class="logo">
                <h1><a href="../"><i class="bi bi-pc-display"></i> SkillaThon - 23</a></h1>
            </div>
            <button class="logout-button"><a href="./logout.php" style="text-decoration: none; color:white;">Logout</a></button>
        </div>
    </header>