<?php
require './_admin_template.php';
require '../includes/Admin.class.php';
require '../includes/Database.class.php';

$conn = Database::getConnection();

Admin::setConnection($conn);
$registrations = Admin::getRegistrations();
if (isset($_GET['del'])) {
    $delId = $_GET['del'];
    Admin::DeleteRegistration($delId);
    header("location: index.php");
}


?>
<br /><br /> <br><br>
<div class="container mt-5">
    <div class="row mb-3">
        <div class="col-md-6 mb-3">
            <h2>Welcome Back.. <?php echo Session::getUsername(); ?></h2>
        </div>
        <div class="col-md-6 text-center">
            <div class="row">
                <!-- Total Payment Card -->
                <div class="col-md-6">
                    <div class="card info-card">
                        <div class="card-body">
                            <i class="fas fa-rupee-sign"></i>
                            <h5 class="card-title">Total Payment</h5>
                            <p class="card-text">Rs. <?php echo Admin::calculateTotalAmount(); ?></p>
                        </div>
                    </div>
                </div>
                <!-- Total Users Card -->
                <div class="col-md-6">
                    <div class="card info-card">
                        <div class="card-body">
                            <i class="fas fa-users"></i>
                            <h5 class="card-title">Total Team</h5>
                            <p class="card-text"><?php echo Admin::TotalCount(); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card table-card">
        <div class="card-header">
            <div class="d-flex justify-content-between align-items-center">
                <div class="input-group w-50">
                    <input type="text" class="form-control" aria-label="Dollar amount (with dot and two decimal places)" id="search-input" placeholder="Search here...">
                    <span class="input-group-text"><i class="bi bi-search"></i></span>
                </div>
                <div class="dropdown">
                    <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        Action
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item" href="./pdf_generate.php?download_pdf=true">Download PDF</a></li>
                        <li><a class="dropdown-item" href="./pdf_generate.php?download_transaction=true">Download Transaction</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="registration-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Unique ID</th>
                            <th>Team Leader Name</th>
                            <th>Institution</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tbody>
                        <?php foreach ($registrations as $index => $registration) : ?>
                            <tr>
                                <td><?php echo $index + 1; ?></td>
                                <td>SKLN<?php
                                        if (strlen($registration['unique_id']) == 3) {
                                            echo '0' . $registration['unique_id'];
                                        } else {
                                            echo $registration['unique_id'];
                                        }
                                        ?></td>
                                <td><?php echo $registration['team_leader_name']; ?></td>
                                <td><?php echo $registration['institution_name']; ?></td>
                                <td><?php echo $registration['email']; ?></td>
                                <td><?php echo $registration['phone']; ?></td>
                                <td>
                                    <a href="../admin/Show_details.php?id=<?php echo $registration['unique_id'] ?>" style="text-decoration: none;"> <button class="btn btn-outline-primary">Show</button></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.0.0-beta1/js/bootstrap.bundle.min.js"></script>
<script>
    function filterTable() {
        var input, filter, table, tr, td, i, j, txtValue;
        input = document.getElementById("search-input");
        filter = input.value.toLowerCase();
        table = document.getElementById("registration-table");
        tr = table.getElementsByTagName("tr");

        for (i = 0; i < tr.length; i++) {
            tr[i].style.display = "none";

            for (j = 0; j < tr[i].cells.length; j++) {
                td = tr[i].cells[j];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toLowerCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                        break;
                    }
                }
            }
        }
    }
    document.getElementById("search-input").addEventListener("input", filterTable);
</script>
</body>

</html>