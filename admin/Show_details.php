<?php
require './_admin_template.php';
require '../includes/Admin.class.php';
require '../includes/Database.class.php';

$conn = Database::getConnection();
Admin::setConnection($conn);

if (isset($_GET['id'])) {
    $unique = $_GET['id'];
    $registrations = Admin::getSingleRegistrations($unique);
}

// $registrations = Admin::getRegistrations();
?>
<style>
    .cc {
        padding: 30px;
        border-radius: 8px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
    }

    .form-label {
        font-weight: bold;
    }
</style> <br><br><br>
<div class="container cc mt-5">
    <a href="javascript:history.go(-1)" class="btn btn-secondary">Back</a>
    <a href="../admin/index.php?del=<?php echo $unique; ?>" class="btn btn-danger">Delete</a>
    <h4 class="mb-4 text-center">Team Id: SKLN<?php
                                                if (strlen($registrations['unique_id']) == 3) {
                                                    echo '0' . $registrations['unique_id'];
                                                } else {
                                                    echo $registrations['unique_id'];
                                                }
                                                ?></h4>
    <div class="mb-3 row">
        <div class="col-sm-4">
            <label for="name" class="col-form-label">Team Leader Name</label>
            <input type="text" class="form-control" id="name" name="name" disabled value="<?php echo $registrations['team_leader_name'] ?>" />
        </div>
        <div class="col-sm-4">
            <label for="team_member_1_name" class="col-form-label">Team Member 1 Name</label>
            <input type="text" class="form-control" name="team_member_1_name" disabled value="<?php echo $registrations['team_member_1_name'] ?>" />
        </div>
        <div class="col-sm-4">
            <label for="team_member_2_name" class="col-form-label">Team Member 2 Name</label>
            <input type="text" class="form-control" name="team_member_2_name" disabled value="<?php echo $registrations['team_member_2_name'] ?>" />
        </div>
    </div>

    <div class="mb-3 row">
        <div class="col-sm-4">
            <label for="team_member_3_name" class="col-form-label">Team Member 3 Name (Optional)</label>
            <input type="text" class="form-control" name="team_member_3_name" disabled value="<?php

                                                                                                if (empty($registrations['team_member_3_name'])) {
                                                                                                    echo 'This Team Having 3 member';
                                                                                                } else {
                                                                                                    echo $registrations['team_member_3_name'];
                                                                                                } ?>" />
        </div>
        <div class="col-sm-4">
            <label for="email" class="col-form-label">Email</label>
            <input type="email" class="form-control" id="email" name="email" disabled value="<?php echo $registrations['email'] ?>" />
        </div>
        <div class="col-sm-4">
            <label for="phone" class="col-form-label">Phone Number</label>
            <input type="tel" class="form-control" id="phone" name="phone" disabled value="<?php echo $registrations['phone'] ?>" />
        </div>
    </div>
    <div class="mb-3 row">
        <div class="col-sm-4">
            <label for="institution" class="col-form-label">Institution Name</label>
            <input type="text" class="form-control" id="institution" name="institution" disabled value="<?php echo $registrations['institution_name'] ?>" />
        </div>
        <div class="col-sm-4">
            <label for="branch" class="col-form-label">Branch</label>
            <input type="text" class="form-control" id="branch" name="branch" disabled value="<?php echo $registrations['branch'] ?>" />
        </div>
        <div class="col-sm-4">
            <label for="degree" class="col-form-label">Degree</label>
            <input type="text" class="form-control" id="degree" name="degree" disabled value="<?php echo $registrations['degree'] ?>" />
        </div>
    </div>

    <div class="mb-3 row">
        <div class="col-sm-4">
            <label for="theme" class="col-form-label">Theme</label>
            <input type="text" class="form-control" id="degree" name="theme" disabled value="<?php echo $registrations['theme'] ?>" />
        </div>
        <div class="col-sm-4">
            <label for="theme" class="col-form-label">Amount</label>
            <input type="text" class="form-control" id="degree" name="theme" disabled value="<?php echo Admin::calculateUserAmount($unique) ?>" />
        </div>
        <div class="col-sm-4">
            <label for="theme" class="col-form-label">Transaction_id</label>
            <input type="text" class="form-control" id="degree" name="theme" disabled value="<?php echo $registrations['transaction_id'] ?>" />
        </div>
    </div>
    <div class="mb-3 row">
        <div class="col-sm-4">
            <label for="image" class="col-form-label">Screenshot of the Payment</label> <br>
            <a href="../<?php echo $registrations['payment_screenshot_filename']; ?>" download class="mt-5">
                Download ScreenShot
            </a>
        </div>
    </div>
</div>