<?php
require './_templates/head.php';
require './_templates/header.php';
require './_templates/hero.php';
?>
<main id="main">
  <?php
  require './_templates/about.php';
  require './_templates/sponser.php';
  require './_templates/highlight.php';
  require './_templates/Timeline.php';
  require './_templates/theme.php';
  require './_templates/contact.php';
  ?>
</main>
<?php
require './_templates/footer.php';
require './_templates/vendor.php';
?>