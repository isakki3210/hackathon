<section id="contact" class="contact">
    <div class="container">
        <div class="section-title" data-aos="fade-up">
            <h2>Contact Us</h2>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
                <div class="contact-about">
                    <h3>SkillaThon - 2k23</h3>
                    <p>You can contact us at any time.</p>
                    <div class="social-links">
                        <a href=" https://www.facebook.com/profile.php?id=61551881603513" target="_blank" class="facebook"><i class="bi bi-facebook"></i></a>
                        <a href="https://www.instagram.com/cse_esec" class="instagram" target="_blank"><i class="bi bi-instagram"></i></a>
                        <a href="https://twitter.com/cse_esec" class="linkedin" target="_blank"><i class="bi bi-twitter"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="200">
                <div class="info">
                    <div>
                        <i class="ri-map-pin-line"></i>
                        <p>Erode Sengunthar Engineering College, <br>Thudupathi, Erode - 628 057</p>
                    </div>

                    <div>
                        <i class="ri-mail-send-line"></i>
                        <p>skillathoncse@gmail.com</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12" data-aos="fade-up" data-aos-delay="300">
                <div class="info">
                    <div>
                        <i class="ri-user-line"></i>
                        <p>Student Incharge<br>B. Karthikeyan - 9361328417</p>
                    </div>
                    <div>
                        <i class="ri-user-line"></i>
                        <p>Student Incharge<br>A. Tharun Kumar - 7603821703</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12" data-aos="fade-up" data-aos-delay="300">
                <div class="info">
                    <div>
                        <i class="ri-user-line"></i>
                        <p>Student Co-ordinator<br>K. Esakkimuthu - 9566831215</p>
                    </div>
                    <div>
                        <i class="ri-user-line"></i>
                        <p>Student Co-ordinator<br>T. C. Stalin - 8637670543</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>