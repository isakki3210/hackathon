<section id="about" class="about">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>About Us</h2>
        </div>
        <div class="row content" style="text-align: justify;">
          <div class="col-lg-6" data-aos="fade-up" data-aos-delay="150">
            <ul>
              <li><i class="ri-check-double-line"></i>Welcome to the State-Level Hackathon <b> SkillaThon - 2k23</b> organized by the Department of Computer Science and Engineering (CSE) at 
                <b>
                  Erode Sengunthar Engineering College!
                </b>In Association with
                <b>IT EXPERT TRAINING</b> 
                <!-- IT EXPERT TRAINING -->
                , Chennai</li>
              <li><i class="ri-check-double-line"></i> We are thrilled to invite you to an exciting Three-Day Hackathon dedicated 
                to fostering innovation, creativity, and collaboration in the field of Technology and Computer Science.</li>
              <li><i class="ri-check-double-line"></i> Our mission is to provide a platform for aspiring tech enthusiasts, programmers, 
                and problem solvers from across the state to come together, share ideas, and build innovative solutions to Real-World Challenges.</li>
                <li><i class="ri-check-double-line"></i>We are committed to nurturing the next generation of tech enthusiasts,
                 problem solvers, and innovators. </li>
            </ul>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-up" data-aos-delay="300">            
            <ul>
              <li><i class="ri-check-double-line"></i>The CSE Department at <b>Erode Sengunthar Engineering College </b> is at the 
                forefront of cutting-edge research and education in the field of Computer Science and Engineering.</li>
              <li><i class="ri-check-double-line"></i>The Department of Computer Science and Engineering has been successfully functioning since 1998. It offers B.E. (Computer Science and Engineering) and M.E. (Computer Science and Engineering). The MoU signed by the Department are CISCO, Infosys (Campus Connect), MICROSOFT (Campus Agreement) and ORACLE.</li>
              <li><i class="ri-check-double-line"></i>The Department is Accredited by NBA, AICTE  since 2009. The department comprises of 7 centralized air conditioned Computer Centers with 300 systems, state of the art computing facilities with sufficient power supply backup. </li>
            </ul>
          </div>
        </div>
      </div>
    </section>