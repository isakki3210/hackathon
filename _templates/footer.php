<footer id="footer">
  <div class="container">
    <div class="row d-flex align-items-center">
      <div class="col-lg-12 text-lg-left text-center">
        <div class="copyright">
          &copy; Copyright <strong>SkillaThon</strong>. All Rights Reserved
        </div>
        Designed by <a href="">CSE</a>
      </div>
    </div>
  </div>
  </div>
</footer>
<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>