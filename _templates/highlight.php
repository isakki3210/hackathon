<div class="section-title" data-aos="fade-up">
    <h2>SkillaThon - 2k23</h2>
</div>
<section id="services" class="services">
    <div class="container">
        <div class="row">
            <?php
            $iconBoxes = array(
                array(
                    "icon" => "bx bx-trophy",
                    "title" => "Grand Prize",
                    "description" => "Get ready to compete for the Attractive Prizes – the ultimate reward for your innovation and problem-solving skills",
                ),
                array(
                    "icon" => "bx bx-file",
                    "title" => "Versatility",
                    "description" => "Explore limitless possibilities with our hackathon, where you can choose from a wide range of themes!",
                ),
                array(
                    "icon" => "bx bx-tachometer",
                    "title" => "48 Hours",
                    "description" => "48 Hours of Non-Stop Development Innovation! Unlocking Boundless Creativity for a Weekend Marathon of Innovation!",
                ),
                array(
                    "icon" => "bx bx-world",
                    "title" => "Interdisciplinary Hackathon",
                    "description" => "Participants from all streams of Engineering to ignite innovation and collaboration.",
                ),
            );
            foreach ($iconBoxes as $box) {
                echo '<div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">';
                echo '    <div class="icon-box" data-aos="fade-up" data-aos-delay="100">';
                echo '        <div class="icon"><i class="' . $box['icon'] . '"></i></div>';
                echo '        <h4 class="title"><a href="">' . $box['title'] . '</a></h4>';
                echo '        <p class="description">' . $box['description'] . '</p>';
                echo '    </div>';
                echo '</div>';
            }
            ?>

        </div>
    </div>
</section>