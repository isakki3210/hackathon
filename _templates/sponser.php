<section id="about" class="about">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Sponsor</h2>
        </div>
        <div class="row content" style="text-align: justify;">
          <div class="col-lg-3" data-aos="fade-up" data-aos-delay="150">
                <img src="../assets/img/it.jpeg" alt="" width="200px" class="rounded-5">
          </div>
          <div class="col-lg-9 pt-4 pt-lg-0" data-aos="fade-up" data-aos-delay="300">            
            <ul>
              <li><i class="ri-check-double-line"></i>IT Expert Training is a hybrid learning platform, dedicated to educate and train students on latest Industry 4.0 skills. </li>
              <li><i class="ri-check-double-line"></i>We are working towards a mission on creating student entrepreneurs and help nation become the superpower. </li>
              <li><i class="ri-check-double-line"></i>We cover major domains like - Internet of Things, Datascience, AI, cybersecurity and Full stack development.</li>
            </ul>
          </div>
        </div>
      </div>
    </section>