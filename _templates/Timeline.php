<div class="section-title" data-aos="fade-up">
    <h2>Time Line</h2>
</div>
<section data-aos="fade-up" data-aos-delay="400">
    <div class="container">
        <div class="main-timeline">
            <?php
            $timelineData = array(
                array(
                    "date" => "29 Sep 2023",
                    "text" => "🚀 Registration Blast-off! Get set for liftoff as we open registrations for the SkillaThon 2k23 - Your journey to innovation begins here!",
                ),
                array(
                    "date" => "15 Oct 2023",
                    "text" => "🛑 Registration Closes: The countdown has begun! Registration is now closed. Stay tuned for mission updates and challenges.",
                ),
                array(
                    "date" => "18 Oct 2023",
                    "text" => "🌟 Launch Day & First Challenge: The launchpad is ready, and the first challenge awaits you. Prepare for an epic lift-off into the hackathon galaxy!",
                ),
                array(
                    "date" => "19 Oct 2023",
                    "text" => "🛰️ 48-Hour Orbit: The hacking marathon is in full orbit! For the next 48 hours, let your creativity soar and explore the vast universe of innovation.",
                ),
                array(
                    "date" => "20 Oct 2023",
                    "text" => "🌌 Third Challenge: As the sun sets, the third challenge rises. Engage your coding thrusters and reach for the stars of success!",
                ),
                array(
                    "date" => "20 Oct 2023",
                    "text" => "🌠 Prize Constellation: The evening of glory is here! Join us as we unveil the constellations of winners and celebrate your cosmic achievements!",
                ),
            );
            $side = "left";
            foreach ($timelineData as $data) {
                echo '<div class="timeline ' . $side . '" data-aos="fade-' . ($side == "left" ? "right" : "left") . '" data-aos-delay="400" >';
                echo '    <div class="card">';
                echo '        <div class="card-body p-4">';
                echo '            <h3>' . $data['date'] . '</h3>';
                echo '            <p class="mb-0 text-secondary">' . $data['text'] . '</p>';
                echo '        </div>';
                echo '    </div>';
                echo '</div>';
                $side = ($side == "left") ? "right" : "left";
            }
            ?>
        </div>
    </div>
</section>