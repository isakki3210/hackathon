<style>
    .swiper-slide {
        text-align: center;
        display: flex;
        justify-content: center;
        align-items: center;
    }
</style>
<div class="section-title" data-aos="fade-up">
    <h2>Themes</h2>
    <p>Championing Innovation, No Matter the Size</p>
</div>
<div class="swiper mySwiper container" data-aos="fade-up" data-aos-delay="100">
    <div class="swiper-wrapper">
        <?php
        $Themes = array(
            array(
                "img" => "smart_city.png",
                "title" => "Smart Cities and Urban Development",
                "description" => "Encourage participants to develop innovative solutions for making cities smarter, more sustainable, and better connected. Focus on improving urban infrastructure, transportation, waste management, and energy efficiency.",
            ),
            array(
                "img" => "hm.png",
                "title" => "Healthcare and Medical Technology",
                "description" => "Encourage participants to develop innovative solutions for making cities smarter, more sustainable, and better connected. Focus on improving urban infrastructure, transportation, waste management, and energy efficiency.",
            ),
            array(
                "img" => "ag.jpg",
                "title" => "Agriculture and Rural Development",
                "description" => "Explore ways to improve agricultural practices, increase crop yields, and address rural development challenges. Topics could include precision agriculture, farm management systems, and sustainable farming techniques.",
            ),
            array(
                "img" => "ai.jpg",
                "title" => "Algorithmic Challenges",
                "description" => "Challenge participants to design and implement efficient algorithms to solve complex computational problems. Topics can include graph algorithms, sorting algorithms, and optimization problems.",
            ),
            array(
                "img" => "aisg.png",
                "title" => "Artificial Intelligence (AI) for Social Good",
                "description" => "Encourage participants to use AI and machine learning to address social and humanitarian challenges, such as healthcare diagnostics, disaster response, or poverty alleviation.",
            ),
            array(
                "img" => "arvr.jpg",
                "title" => "Virtual Reality (VR) and Augmented Reality (AR) Experiences",
                "description" => "Inspire participants to build immersive VR/AR applications for education, entertainment, or training purposes.",
            ),
            array(
                "img" => "ait.png",
                "title" => "Accessibility and Inclusion Tech",
                "description" => "Task participants with developing technology solutions that enhance accessibility for people with disabilities or promote inclusivity in various areas of life.",
            ),
            array(
                "img" => "open-innovation.png",
                "title" => "Open Innovation Challenge",
                "description" => "Participants to choose their own problem statements and solutions, promoting creativity and versatility. This theme can be open-ended, allowing a wide range of ideas and projects.",
            ),
        );
        foreach ($Themes as $theme) {
        ?>
            <div class="swiper-slide">
                <div class="card" style="width: 24rem; height: 30rem; box-shadow: 0 0 29px 0 rgba(68, 88, 144, 0.12);
          ">
                    <img src="./assets/img/Themes/<?php echo $theme['img']; ?>" class="card-img-top" alt="..." height="250px">
                    <div class="card-body text-center" style="padding: 15px 15px 20px 20px">
                        <h5 class="card-title text-dark"><?php echo $theme['title']; ?></h5>
                        <p class="card-text text-secondary" style="font-size: 14px; line-height: 28px;"><?php echo $theme['description']; ?></p>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div> <br> <br>
    <div class="swiper-pagination"></div>
</div>