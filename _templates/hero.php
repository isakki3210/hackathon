<section id="hero" class="d-flex align-items-center">

  <div class="container">
    <div class="row">
      <div class="col-lg-6 pt-lg-0 order-1 order-lg-1 d-flex flex-column justify-content-center">
        <h2 data-aos="fade-up" style="font-size: 30px; color:white;">Skillathon 2K23 - Unleash Innovation at the State's Forefront</h2>
        <p data-aos="fade-up" data-aos-delay="400" style="font-size: 18px; color:white;">Presented by the Department of Computer Science and Engineering Hosted at Erode Sengunthar Engineering College</p>
        <div data-aos="fade-up" data-aos-delay="800">
          <a href="Reg.php" class="btn-get-started scrollto mt-4">Register</a>
        </div>
      </div>
      <div class="col-lg-6 order-2 order-lg-2 hero-img" data-aos="fade-left" data-aos-delay="200">
        <img src="assets/img/main.png" class="img-fluid animated" alt="">
      </div>
    </div>
  </div>
</section>