<?php
require './_templates/head.php';
require './_templates/header.php';
?>
<title>Registration Success</title>
<style>
    .box {
        margin-top: 60px;
        display: flex;
        justify-content: space-around;
        flex-wrap: wrap;
    }

    .alert {
        margin-top: 25px;
        font-size: 25px;
        font-family: sans-serif;
        text-align: center;
        width: 300px;
        height: 100px;
        padding-top: 150px;
        position: relative;
    }

    .alert::before {
        width: 100px;
        height: 100px;
        position: absolute;
        inset: 20px 0px 0px 100px;
        font-size: 60px;
        line-height: 100px;
        border: 5px solid gray;
        animation-name: reveal;
        animation-duration: 1.5s;
        animation-timing-function: ease-in-out;
    }

    .alert>.alert-body {
        opacity: 0;
        animation-name: reveal-message;
        animation-duration: 1s;
        animation-timing-function: ease-out;
        animation-delay: 1.5s;
        animation-fill-mode: forwards;
    }

    @keyframes reveal-message {
        from {
            opacity: 0;
        }

        to {
            opacity: 1;
        }
    }

    .success {
        color: green;
    }

    .success::before {
        content: '✓';
        background-color: #eff;
        box-shadow: 0px 0px 12px 7px rgba(200, 255, 150, 0.8) inset;
        border: 5px solid green;
        border-radius: 50%;
    }

    .error {
        color: red;
    }

    .error::before {
        content: '✗';
        background-color: #fef;
        box-shadow: 0px 0px 12px 7px rgba(255, 200, 150, 0.8) inset;
        border: 5px solid red;
        border-radius: 50%;
    }

    @keyframes reveal {
        0% {
            border: 5px solid transparent;
            color: transparent;
            box-shadow: 0px 0px 12px 7px rgba(255, 250, 250, 0.8) inset;
            transform: rotate(1000deg);
        }

        25% {
            border-top: 5px solid gray;
            color: transparent;
            box-shadow: 0px 0px 17px 10px rgba(255, 250, 250, 0.8) inset;
        }

        50% {
            border-right: 5px solid gray;
            border-left: 5px solid gray;
            color: transparent;
            box-shadow: 0px 0px 17px 10px rgba(200, 200, 200, 0.8) inset;
        }

        75% {
            border-bottom: 5px solid gray;
            color: gray;
            box-shadow: 0px 0px 12px 7px rgba(200, 200, 200, 0.8) inset;
        }

        100% {
            border: 5px solid gray;
            box-shadow: 0px 0px 12px 7px rgba(200, 200, 200, 0.8) inset;
        }
    }
</style>
</head>

<body>
    <?php
    if (isset($_GET['msg'])) {
        $msg = $_GET['msg'];
    }
    if ($msg == 'email' || $msg == 'phone' || $msg == 'transaction_id') {
    ?>
        <div class="card-body text-center d-flex flex-column align-items-center" style="margin: 200px auto;">
            <h5 class="m-3">Error !!</h5>
            <div id="lottie-containers" style="width: 200px; height: 200px;"></div>
            <h6 class="m-3 text-capitalize"><?php echo $msg; ?> Already Registered</h6>
            <p class="text-muted">It seems that the <?php echo $msg; ?>, you provided is already associated with an existing account.</p>
            <a href="./Reg.php" class="mt-3">
                <button type="submit" class="btn btn-danger text-light" style=" width: 100%;">Back to Register</button>
            </a>
        </div>
    <?php } else if ($msg == 'otp') {
    ?>
        <div class="card-body text-center d-flex flex-column align-items-center" style="margin: 200px auto;">
            <h5 class="m-3">Error !!</h5>
            <div id="lottie-containers" style="width: 200px; height: 200px;"></div>
            <h6 class="m-3">Please Check Your OTP..Registration Required</h6>
            <p class="text-muted">We've sent a one-time password to your registered email address Please check your inbox for the OTP.</p>
            <a href="./Reg.php" class="mt-3">
                <button type="submit" class="btn btn-danger text-light" style=" width: 100%;">Back to Register</button>
            </a>
        </div>
    <?php
    } else if ($msg == 'name') {
    ?>
        <div class="card-body text-center d-flex flex-column align-items-center" style="margin: 200px auto;">
            <h5 class="m-3">Error !!</h5>
            <div id="lottie-containers" style="width: 200px; height: 200px;"></div>
            <h6 class="m-3 text-capitalize"><?php echo $msg; ?> Already Registered</h6>
            <p class="text-muted">It seems that the <?php echo $msg; ?>, you provided is already associated with an existing account at the same Institution</p>
            <a href="./Reg.php" class="mt-3">
                <button type="submit" class="btn btn-danger text-light" style=" width: 100%;">Back to Register</button>
            </a>
        </div>
    <?php
    } else { ?>
        <div class="card-body text-center d-flex flex-column align-items-center" style="margin: 200px auto;">
            <h5 class="m-3">Thanks for registering.</h5>
            <div id="lottie-container" style="width: 200px; height: 200px;"></div>
            <h6 class="m-3">Your Registration was Successful for SkillaThon 2K23 <br> Now you can Check Your mail</h6>
            <a href="index.php" class="mt-3">
                <button type="submit" class="btn btn-success linear-green" style="background-color: #2DB65A; border-color: #2DB65A; width: 100%;">Back to Home</button>
            </a>
        </div>
    <?php } ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.7.0/lottie.min.js"></script>
    <script>
        // Initialize the Lottie animation
        const animation = lottie.loadAnimation({
            container: document.getElementById('lottie-container'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: 'https://assets2.lottiefiles.com/packages/lf20_lk80fpsm.json'
        });

        // Load and play the Lottie animation
        const animations = lottie.loadAnimation({
            container: document.getElementById('lottie-containers'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: './assets/img/Animation - 1695782054449.json',
        });
    </script>


</body>

</html>