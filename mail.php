<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

$mail = new PHPMailer(true); // Create a new PHPMailer instance

try {
    // Server settings
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'skillathoncse@gmail.com';
    $mail->Password = 'xfpf udsa deuw hjdy';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    // Recipient
    $mail->setFrom('skillathoncse@gmail.com', 'Skillathon2k23');
    $mail->addAddress('recipient@example.com', 'Recipient Name');

    // Content
    $mail->isHTML(true);
    $mail->Subject = 'State-Level Hackathon Registration Successful!';
    $mail->Body    = "We are thrilled to inform you that your registration for the State-Level Hackathon has been successfully completed. Your unique participant ID is: [Unique ID].

    Congratulations! You are now officially part of this exciting competition. Get ready to showcase your skills, collaborate with fellow enthusiasts, and take on challenges that will push your limits.
    
    Please keep an eye on your inbox, as we will be sending a confirmation email shortly. This email will contain important details about the event schedule, rules, and additional resources to help you prepare effectively.
    
    If you have any immediate questions or concerns, feel free to reach out to our support team at [Support Email Address].
    
    Once again, welcome to the State-Level Hackathon, and best of luck in the upcoming competition!
    
    Warm regards,
    
    [Your Organization's Name]
    [Contact Information]";

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}
